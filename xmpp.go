package mailalert

import (
	"bufio"
	"bytes"
	"fmt"
	"net"
	"strings"
	"text/template"

	"github.com/processone/gox/xmpp"
)

// ComposeMessage compose a message based on template and envelop information
func ComposeMessage(msgTemplate string, envelop Envelop) (string, error) {
	tmpl := template.Must(template.New("notification").Parse(msgTemplate))
	var buffer bytes.Buffer
	writer := bufio.NewWriter(&buffer)
	err := tmpl.Execute(writer, envelop)
	if err != nil {
		return "", err
	}

	writer.Flush()
	return buffer.String(), nil
}

// SendMessage sends the message to the specific JID
func SendMessage(config Config, jid string, message string) error {
	if config.Server.Host == "" {
		domain := strings.Split(jid, "@")[1]
		_, srvs, err := net.LookupSRV("xmpp-client", "tcp", domain)
		if err != nil {
			return err
		}
		config.Server.Host = srvs[0].Target
		config.Server.Port = srvs[0].Port
	}

	if config.Server.Port == 0 {
		config.Server.Port = 5222
	}

	options := xmpp.Options{
		Address:  config.Server.Host + ":" + fmt.Sprint(config.Server.Port),
		Jid:      config.Client.Username,
		Password: config.Client.Password,
	}
	var client *xmpp.Client
	var err error
	if client, err = xmpp.NewClient(options); err != nil {
		return err
	}
	if _, err = client.Connect(); err != nil {
		return err
	}
	msg := xmpp.ClientMessage{
		Packet: xmpp.Packet{
			To:   jid,
			From: config.Client.Username,
		},
		Body: message,
	}
	return client.Send(msg.XMPPFormat())
}

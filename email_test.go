package mailalert

import (
	"os"
	"strings"
	"testing"
)

type emailTestResult struct {
	Envelop
	ErrorPattern string
}

var emailTestCases = map[string]emailTestResult{
	"tests/empty-subject.eml": {
		Envelop{
			"re@uid.com",
			"demo@example.com",
			"",
		},
		"",
	},
	"tests/multi-line-encoded.eml": {
		Envelop{
			"kpe@egd.com",
			"demo@example.com",
			"新劳动法实操应对及违纪员工处理",
		},
		"",
	},
	"tests/multi-line-mixed.eml": {
		Envelop{
			"ix@chinacyber.com.cn",
			"demo@example.com",
			"***SPAM*** 基于信息化與知識管理的現代企業檔案管理 dF",
		},
		"",
	},
	"tests/multi-line-plain.eml": {
		Envelop{
			"vargas785@db.biz",
			"demo@example.com",
			"***SPAM*** This is your opportunity to get a 20 bagger in the market very fast",
		},
		"",
	},
	"tests/single-line-mixed.eml": {
		Envelop{
			"aqe@wyt.com",
			"demo@example.com",
			"***SPAM*** 全能型车间主任",
		},
		"",
	},
	"tests/single-line-plain.eml": {
		Envelop{
			"heath99736@sparknetbd.com",
			"demo@example.com",
			"***SPAM*** This company's being acquired tomorrow",
		},
		"",
	},
	"tests/invalid-charset.eml": {
		Envelop{
			"",
			"",
			"",
		},
		"unhandled charset",
	},
}

func TestParseEmail(t *testing.T) {
	oldStdin := os.Stdin
	oldStdout := os.Stdout
	stdout, err := os.Open("/dev/null")
	if err != nil {
		t.Errorf("Cannot open /dev/null: %s\n", err)
		return
	}
	os.Stdout = stdout
	for testFile, expected := range emailTestCases {
		stdin, err := os.Open(testFile)
		if err != nil {
			t.Errorf("Cannot open test case [%s]: %s\n", testFile, err)
			continue
		}
		os.Stdin = stdin
		envelop, err := ParseEmail()
		if err == nil && expected.ErrorPattern != "" {
			t.Errorf("Expected [%s], but got %v\n", expected.ErrorPattern, nil)
		} else if err != nil {
			if expected.ErrorPattern == "" {
				t.Errorf("Expected %v, but got %v\n", nil, err)
			} else if !strings.Contains(err.Error(), expected.ErrorPattern) {
				t.Errorf("Expected [%s], but got %v\n",
					expected.ErrorPattern, nil)
			}
		} else if envelop != expected.Envelop {
			t.Errorf("Expected %v, but got %v\n", expected, envelop)
		}
	}
	os.Stdin = oldStdin
	os.Stdout = oldStdout
}

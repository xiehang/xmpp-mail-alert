package mailalert

import (
	"bufio"
	"fmt"
	"mime"
	"os"
	"regexp"
	"strings"
)

// Envelop contains information that will be notified to addressee
type Envelop struct {
	From    string
	To      string
	Subject string
}

func trim(src string) string {
	return strings.Trim(src, " \t\n\r")
}

// ParseEmail parses email (from STDIN) and return envelop information include
// From, To, and Subject. According to maildrop protocol, raw email message
// needs to be output to STDOUT
func ParseEmail() (Envelop, error) {
	reEmail := `(?P<email>[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,})`
	reFromHeader := regexp.MustCompile(`(?is)^From:.*?` + reEmail + `.*`)
	reToHeader := regexp.MustCompile(`(?is)^To:.*?` + reEmail + `.*`)
	reSubjectHeader := regexp.MustCompile(`(?is)^Subject:(?P<subject>.*)`)

	rawEmail := bufio.NewReader(os.Stdin)
	envelop := Envelop{}
	processingSubject := false
	decoder := new(mime.WordDecoder)
	for {
		line, err := rawEmail.ReadString('\n')
		if err != nil {
			break
		}
		fmt.Print(line)

		if envelop.From == "" {
			mailFrom := reFromHeader.ReplaceAllString(line, "${email}")
			if mailFrom != line {
				envelop.From = strings.ToLower(mailFrom)
				continue
			}
		}

		if envelop.To == "" {
			sendTo := reToHeader.ReplaceAllString(line, "${email}")
			if sendTo != line {
				envelop.To = strings.ToLower(sendTo)
				continue
			}
		}

		if envelop.Subject == "" {
			subject := reSubjectHeader.ReplaceAllString(line, "${subject}")
			if subject != line {
				envelop.Subject = trim(subject)
				processingSubject = true
				continue
			}
		}

		if processingSubject {
			// subject can be multiple lines
			if strings.HasPrefix(line, " ") || strings.HasPrefix(line, "\t") {
				envelop.Subject += " " + trim(line)
			} else {
				processingSubject = false
				// set subject to space to avoid parsing email excessively
				if envelop.Subject == "" {
					envelop.Subject = " "
				}
			}
		}
	}
	decoded, err := decoder.DecodeHeader(trim(envelop.Subject))
	if err != nil {
		envelop.Subject = ""
		return envelop, err
	}
	envelop.Subject = decoded
	return envelop, nil
}

package mailalert

import (
	"strings"
	"testing"
)

func TestLoadConfig(t *testing.T) {
	// non-exist YAML
	configMap, err := LoadConfig("from/nowhere.yaml")
	expected := "such file or directory"
	if err == nil {
		t.Errorf("Expect non-%v, but got %v\n", nil, err)
	} else if !strings.HasSuffix(err.Error(), expected) {
		t.Errorf("Expected ... %v, but got %v\n", expected, err)
	}

	// empty YAML
	configMap, err = LoadConfig("tests/empty.yaml")
	if err != nil {
		t.Errorf("Expect %v, but got %v\n", nil, err)
	}

	// default values
	configMap, err = LoadConfig("tests/default_values.yaml")
	if err != nil {
		t.Errorf("Expect %v, but got %v\n", nil, err)
	}
	if domainConfig, ok := configMap.Domains["example.com"]; !ok {
		t.Errorf("Expect value for [example.com], but got not-found\n")
	} else {
		if domainConfig.Enabled != false {
			t.Errorf("Expect %v, but got %v\n", false, domainConfig.Enabled)
		}
		if domainConfig.Server.Host != "" {
			t.Errorf("Expect %v, but got %v\n", "", domainConfig.Server.Host)
		}
		if domainConfig.Server.Port != 0 {
			t.Errorf("Expect %v, but got %v\n", 0, domainConfig.Server.Port)
		}
		if domainConfig.Client.Username != "" {
			t.Errorf(
				"Expect %v, but got %v\n", "", domainConfig.Client.Username)
		}
		if domainConfig.Client.Password != "" {
			t.Errorf(
				"Expect %v, but got %v\n", "", domainConfig.Client.Password)
		}
		if domainConfig.Client.Resource != "" {
			t.Errorf(
				"Expect %v, but got %v\n", "", domainConfig.Client.Resource)
		}
		if domainConfig.Message != "" {
			t.Errorf("Expect %v, but got %v\n", "", domainConfig.Message)
		}
	}

	// invalid values
	configMap, err = LoadConfig("tests/invalid_enabled.yaml")
	expected = "yaml: unmarshal errors:"
	if err == nil {
		t.Errorf("Expected non-%v, but got %v\n", nil, err)
	} else if !strings.HasPrefix(err.Error(), expected) {
		t.Errorf("Expected %v ..., but got %v\n", expected, err)
	} else if domainConfig, ok := configMap.Domains["example.com"]; ok {
		t.Errorf(
			"Expect not found for [example.com], but got %v\n", domainConfig)
	}

	configMap, err = LoadConfig("tests/invalid_port.yaml")
	expected = "yaml: unmarshal errors:"
	if err == nil {
		t.Errorf("Expected non-%v, but got %v\n", nil, err)
	} else if !strings.HasPrefix(err.Error(), expected) {
		t.Errorf(
			"Expected %v ..., but got %v\n", expected, err)
	} else if domainConfig, ok := configMap.Domains["example.com"]; ok {
		t.Errorf(
			"Expect not found for [example.com], but got %v\n", domainConfig)
	}

	// good config
	configMap, err = LoadConfig("tests/good.yaml")
	if err != nil {
		t.Errorf("Expect non-%v, but got %v\n", nil, err)
	}
	if domainConfig, ok := configMap.Domains["example.com"]; !ok {
		t.Errorf("Expect value for [example.com], but got not-found\n")
	} else {
		if domainConfig.Enabled != true {
			t.Errorf("Expect %v, but got %v\n", true, domainConfig.Enabled)
		}

		server := domainConfig.Server
		expected = "xmpp.example.com"
		if server.Host != expected {
			t.Errorf("Expect %v, but got %v\n", expected, server.Host)
		}
		if server.Port != 5222 {
			t.Errorf("Expect %v, but got %v\n", 5222, server.Port)
		}

		client := domainConfig.Client
		expected = "mailalert"
		if client.Username != expected {
			t.Errorf("Expect %v, but got %v\n", expected, client.Username)
		}
		expected = "well-i-dont-know"
		if client.Password != expected {
			t.Errorf("Expect %v, but got %v\n", expected, client.Password)
		}
		expected = "Email-Notification"
		if client.Resource != expected {
			t.Errorf("Expect %v, but got %v\n", expected, client.Resource)
		}
		expected = "Mail alert:\nTo: {{.To}}\nFrom: {{.From}}\n{{.Subject}}\n"
		if domainConfig.Message != expected {
			t.Errorf("Expect %v, but got %v\n", expected, domainConfig.Message)
		}
	}
}

package main

import (
	"flag"
	"strings"

	"gitlab.com/xiehang/xmpp-mail-alert"
	"gopkg.in/yaml.v2"
)

var (
	configFile = flag.String("conf", "./notify.yml", "configuration file")
	jid        = flag.String("jid", "", "XMPP user id to notify")
	debug      = flag.Bool("debug", false, "enable debug output")
)

func main() {
	// get command line options
	flag.Parse()
	items := strings.Split(*jid, "@")
	if len(items) != 2 {
		mailalert.ErrorLog.Printf(
			"Need to speficiy a JID to receive the notification\n")
		return
	}
	domain := items[1]

	// make sure there is a suitable configuration for the domain
	config, err := mailalert.LoadConfig(*configFile)
	if err != nil {
		mailalert.ErrorLog.Printf("Cannot load configuration file: %v\n", err)
		return
	}

	userConfig, ok := config.Domains[domain]
	if !ok {
		if catchAll, ok := config.Domains["*"]; ok {
			userConfig = catchAll
		} else {
			mailalert.ErrorLog.Printf(
				"Cannot find configuration for domain [%s]\n", domain)
			return
		}
	}

	// just in case debug is on
	if *debug {
		dump, _ := yaml.Marshal(config)
		mailalert.DebugLog.Printf("Loaded configuration: \n%s\n", dump)

		dump, _ = yaml.Marshal(userConfig)
		mailalert.DebugLog.Printf("Using configuration: \n%s\n", dump)
	}

	// ParseEmail will follow maildrop protocol to output raw email message
	// to STDOUT, at the same time it parse email and get envelop information
	envelop, err := mailalert.ParseEmail()
	if *debug {
		mailalert.DebugLog.Printf("Envelop information: %v\n", envelop)
	}

	// don't do anything if notification config is disabled
	if !userConfig.Enabled {
		return
	}

	message, err := mailalert.ComposeMessage(userConfig.Message, envelop)
	if err != nil {
		mailalert.ErrorLog.Printf("Failed to compose message: %v\n", err)
	} else if *debug {
		mailalert.DebugLog.Printf("About to send to %s:\n %s\n", *jid, message)
	}

	err = mailalert.SendMessage(userConfig, *jid, message)
	if err != nil {
		mailalert.ErrorLog.Printf("Failed to send message: %v\n", err)
	} else if *debug {
		mailalert.DebugLog.Printf("Sent to %s\n", *jid)
	}
}

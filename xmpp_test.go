package mailalert

import (
	"strings"
	"testing"
)

type parameter struct {
	Template string
	Header   Envelop
}

type returnValue struct {
	ErrorPattern string
	Message      string
}

var xmppTestCases = map[parameter]returnValue{
	parameter{"", Envelop{"", "", ""}}:                           returnValue{"", ""},
	parameter{"{{.dummy}}", Envelop{"", "", ""}}:                 returnValue{"can't evaluate field dummy", ""},
	parameter{"w/o {{.From}}", Envelop{"", "", ""}}:              returnValue{"", "w/o"},
	parameter{"w/ {{.From}}", Envelop{"from_email", "", ""}}:     returnValue{"", "w/ from_email"},
	parameter{"w/o {{.To}}", Envelop{"", "", ""}}:                returnValue{"", "w/o"},
	parameter{"w/ {{.To}}", Envelop{"", "to_email", ""}}:         returnValue{"", "w/ to_email"},
	parameter{"w/o {{.Subject}}", Envelop{"", "", ""}}:           returnValue{"", "w/o"},
	parameter{"w/ {{.Subject}}", Envelop{"", "", "the subject"}}: returnValue{"", "w/ the subject"},
}

func TestComposeMessage(t *testing.T) {
	for param, result := range xmppTestCases {
		message, err := ComposeMessage(param.Template, param.Header)
		if result.ErrorPattern == "" {
			if err != nil {
				t.Errorf("Expected [%v], but got [%v]\n", nil, err)
			}
		} else if err == nil {
			if result.ErrorPattern != "" {
				t.Errorf("Expected [%v], but got [%v]\n",
					result.ErrorPattern, nil)
			}
		} else if !strings.Contains(err.Error(), result.ErrorPattern) {
			t.Errorf("Expected [%v], but got [%v]\n", result.ErrorPattern, err)
		} else if message != result.Message {
			t.Errorf("Expected [%s], but got [%s]\n", result.Message, message)
		}
	}
}

func TestSendMessage(t *testing.T) {
	// invalid host
	config := Config{}
	expected := "no such host"
	err := SendMessage(config, "jid@domain.tld", "message")
	if err == nil || !strings.Contains(err.Error(), expected) {
		t.Errorf("Expected [%s], but got [%v]\n", expected, err)
	}

	// invalid host
	config = Config{}
	expected = "missing password"
	err = SendMessage(config, "jid@jabber.org", "message")
	if err == nil || !strings.Contains(err.Error(), expected) {
		t.Errorf("Expected [%s], but got [%v]\n", expected, err)
	}

	// invalid username/password
	config.Client.Username = "username@domain.tld"
	config.Client.Password = "password"
	config.Client.Resource = "resource"
	expected = "stream open decode features:"
	err = SendMessage(config, "jid@jabber.org", "message")
	if err == nil || !strings.Contains(err.Error(), expected) {
		t.Errorf("Expected [%s], but got [%v]\n", expected, err)
	}

	// you can edit following block with credentials for a working XMPP server
	/*
		config.Server.Host = "domain.tld"
		config.Client.Username = "jid1@domain.tld"
		config.Client.Password = "password"
		err = SendMessage(config, "jid2@domain.tld", "test message")
		if err != nil {
			t.Errorf("Expected [%v], but got [%v]\n", nil, err)
		}
	*/
}

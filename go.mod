module gitlab.com/xiehang/xmpp-mail-alert

go 1.12

require (
	github.com/processone/gox v0.0.0-20160314103131-c41ed1c32f62
	gopkg.in/yaml.v2 v2.0.0-20150116202057-bef53efd0c76
)

.PHONY: *

all: mod release

mod:
	env GO111MODULE=on go mod vendor

clean:
	rm -rf build
	rm -rf cover.out cover.html

release:
	mkdir -p build
	env GOOS=linux GOARCH=amd64 go build -mod=vendor -o build/mail-alert cli/mail-alert.go

lint:
	golint ./
	golint cli/

test:
	go test -mod=vendor

coverage:
	go test -mod=vendor --coverprofile cover.out
	go tool cover -html=cover.out -o cover.html

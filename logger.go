package mailalert

import (
	"log"
	"os"
)

var (
	// InfoLog writes informative log messages to stdout
	InfoLog = log.New(os.Stdout, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	// WarningLog writes warning log messages to stdout
	WarningLog = log.New(os.Stderr, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	// ErrorLog writes error log messages to stderr
	ErrorLog = log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	// DebugLog writes debugging log messages to stdout
	DebugLog = log.New(os.Stderr, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
)

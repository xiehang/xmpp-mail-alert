How to deploy:

1. put notify.yaml to /etc/mail/, with necessary changes:
   - XMPP server authentication
   - alert format
2. copy build/mail-alert to a bin directory (such as /usr/local/bin/) and chmod to 755
3. hook up mail-alert with your mail system, either maildrop or procmail

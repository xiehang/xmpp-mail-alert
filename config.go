package mailalert

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

/* sample configuration YAML:
---
domains:
  example.com:
    enabled: true
    server:
      host: xmpp.example.com
      port: 5222
    client:
      username: mailalert
      password: well-i-dont-know
      resource: Email-Notification
    message: |
      Mail alert:
      To: __ADDRESSEE__
      From: __SENDER__
      __SUBJECT__

  '*':
    enabled: false
*/

// Config holds configuration for single domain
type Config struct {
	Enabled bool
	Server  struct {
		Host string
		Port uint16
	}
	Client struct {
		Username string
		Password string
		Resource string
	}
	Message string
}

// ConfigMap contains all configuration for mail alert
type ConfigMap struct {
	Domains map[string]Config `domains`
}

// LoadConfig load configuration from configuration file in YAML format.
func LoadConfig(configFileName string) (ConfigMap, error) {
	var conf ConfigMap
	source, err := ioutil.ReadFile(configFileName)
	if err != nil {
		return ConfigMap{}, err
	}

	err = yaml.Unmarshal(source, &conf)
	if err != nil {
		return ConfigMap{}, err
	}
	return conf, nil
}
